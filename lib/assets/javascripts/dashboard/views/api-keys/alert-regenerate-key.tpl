<div class="CDB-Text Dialog-header u-inner">
  <div class="Dialog-headerIcon Dialog-headerIcon--negative">
    <i class="CDB-IconFont CDB-IconFont-keys"></i>
  </div>
  <p class="Dialog-headerTitle u-ellipsLongText">
    Stai per rigenerare la tua API key
  </p>
  <p class="Dialog-headerText">
    Dovrai aggiornare tutte le app sviluppate con la nuova API key. Sei sicuro di voler continuare?
  </p>
</div>

<div class="Dialog-footer u-inner">
  <button type="button" class="CDB-Button CDB-Button--secondary js-cancel">
    <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-small u-upperCase">Annulla</span>
  </button>
  <button type="button" class="CDB-Button CDB-Button--error js-submit">
    <span class="CDB-Button-Text CDB-Text is-semibold CDB-Size-small u-upperCase">Rigenera l'API key</span>
  </button>
</div>
